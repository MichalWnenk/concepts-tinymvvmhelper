﻿using System;
using System.Windows;
using System.Windows.Media;
using TinyMVVMHelper;

namespace Examples
{
    class SimpleViewModel : ViewModel
    {
        private int clickCount;
        private Brush numberColor;
        
        private Command clickCommand;

        public SimpleViewModel()
        {
            clickCount = 0;
            numberColor = Brushes.Green;
            clickCommand = new Command(DoClick);
        }

        public int ClickCount
        {
            get { return clickCount; }
            set
            {
                clickCount = value;
                FirePropertyChanged("ClickCount");
            }
        }

        public Brush NumberColor
        {
            get { return numberColor; }
            set
            {
                numberColor = value;
                FirePropertyChanged("NumberColor");
            }
        }

        public Command ClickCommand { get { return clickCommand; } }

        private void DoClick(object parameter)
        {
            ClickCount += 1;
            if (ClickCount >= 10)
            {
                MessageBox.Show("Jestem zmęczony. Żegnaj.", "Nagrabiłeś sobie!");
                Application.Current.Dispatcher.InvokeShutdown();
            }
            if (ClickCount >= 8)
                NumberColor = Brushes.Red;
            else if (ClickCount >= 5)
                NumberColor = Brushes.Goldenrod;
        }

    }
}
